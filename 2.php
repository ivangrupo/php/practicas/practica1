<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Ejercicio 2</title>
    </head>
    <body>
        <table width="100%" border="1">
            <tr>
                <td>
                     <?php 
                    // Cuando utilicéis  la instrucción echo podéis utilizar las comillas dobles o simples
                    echo 'Este texto quieroque lo escribas utilizando la función echo de php';
                    ?>
                </td>
                <td>
                  Aquí debecolocar un texto directamente en HTML.  
                </td>
            </tr>
            <tr>
                <td>
                    <?php 
                    print 'Este texto quiero que lo escribas utilizando la función print de php';
                    ?>
                </td>
                <td>
                    <?php 
                    echo "Academia Alpe.";
                    ?>
                </td>
            </tr>

        </table>

        <?php
        // put your code here
        ?>
    </body>
</html>
