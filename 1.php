<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Ejercicio 1</title>
    </head>
    <body>
        En este ejercicio vamos a escribir un texto en la página web a través de PHP
        <hr>
        <?php
        // Esto es un comentario
        echo 'Este texto está escrito a desde el script de PHP';
        /*Esto es otro comentario 
          pero con varias líneas*/
       echo 'Este texto también se escribe desde el script de php.';
        ?>
        <hr>
        Esto está escrito en HTML normal
        <hr>
        <?php
        #########################################################################
        ################## Este comentario es de una sola línea ################# 
        #########################################################################
        
        # En una página podemos colocar tantos scripts de php como se desee
        print("Esra es otra forma de escribir cosas en la web");
        ?>
    </body>
</html>
